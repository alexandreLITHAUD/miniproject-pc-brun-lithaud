package prodcons.v6;

public class ProdConsBuffer implements IProdConsBuffer {
	//pointeurs sur les positions d'ajout et de suppr
	private int in, out;

	//nombre de places libres dans la structure
	private int nFree;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;

	// tableau des messages
	private Message[] msg;
	
	//bools de gestion de synchro
	private boolean InPutK;
	private int InGetK = 0;
	private int lockGetByMultiple = 0;
	
	public ProdConsBuffer(int bufSz) {
		this.bufSz = bufSz;
		nFree = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
		InPutK = false;

	}

	//fonction donnant le nombre de cases occupés
	private int getNOccupied() {
		return bufSz - nFree;
	}

	@Override
	public synchronized void put(Message m) throws InterruptedException {
		//tq il n'y a pas de places libres ou qu'un putK est en cours, on attends
		while (!(nFree > 0) || InPutK) {
			wait();
		}
		
		//traitement d'un message
		msg[in] = m;
		in = (in + 1) % bufSz;
		nMsgGlobal++;
		//on informe le message qu'il a été placé une fois dans le buffer
		m.addOneInBuffer();
		//on a ajouté un élément
		nFree--;
		
		//réveil des autres threads
		notifyAll();
	}

	
	@Override
	public synchronized void put(Message m, int n) throws InterruptedException {
		//tant qu'il y a deja quelqu'un dans un putK
		while (InPutK) {
			wait();
		}
		//entrée dans le putK
		InPutK = true;
		
		// On depose n fois le message
		for (int i = 0; i < n; i++) {
			//verification de l'espace
			while (!(nFree > 0)) {
				wait();
			}
			//ajout du message 
			msg[in] = m;
			in = (in + 1) % bufSz;
			nMsgGlobal++;
			m.addOneInBuffer();
			nFree--;
			//informatioon qu'un message est arrivé
			notifyAll();
		}
		//sortie du putK
		InPutK = false;
		notifyAll();
		
		//attente que tous les exemplaires soient consommés
		while (m.getNumberInBuffer() > 0) {
			wait();
		}
		notifyAll();
	}

	@Override
	public synchronized Message get() throws InterruptedException {
		// Tq il n'y a aucun élément ou qu'un thread est dans le qetK (non bloqué suite a la lecture d'un message de putK)
		while ((!(getNOccupied() > 0)) || (InGetK>0 && (lockGetByMultiple != InGetK))) {
			wait();
		}
		
		//getion du message 
		Message m = msg[out];
		out = (out + 1) % bufSz;
		m.takeOneInBuffer();
		//libération de l'espace
		nFree++;
		//pour le debug, on indique qu'on a bien consommer un msg
		Consumer.incr();
		
		notifyAll();
		
		//si le message lu est un putK, on attends qu'il soit entierement consommé
		while (m.getNumberInBuffer() > 0) {
			wait();
		}
		notifyAll();
		return m;
	}

	@Override
	public synchronized Message[] get(int k) throws InterruptedException {
		//tq quelqu'un est deja dans le getK et qu'il n'est pas bloqué par un getK
		while ((InGetK>0 && (lockGetByMultiple != InGetK))) {
			wait();
		}
		
		//entrée
		InGetK ++;
		Message[] messagesK = new Message[k];
		//on recupère k messages :
		for (int i = 0; i < k; i++) {
			//si on peut récup
			while (!(getNOccupied() > 0)) {
				wait();
			}
			//on recupère
			messagesK[i] = msg[out];
			out = (out + 1) % bufSz;
			messagesK[i].takeOneInBuffer();
			nFree++;
			//pour le debug, on indique qu'on a bien consommer un msg
			Consumer.incr();
			//libération des producteurs
			notifyAll();
			
			//si le message qui vient d'être recup est d'un getK
			while (messagesK[i].getNumberInBuffer() > 0) {
				lockGetByMultiple ++;
				wait();
				lockGetByMultiple --;
			}
			//on n'est plus bloqué dans le putK
			notifyAll();
		}
		//sortie du getK
		InGetK --;
		notifyAll();
		return messagesK;
	}

	@Override
	public int nmsg() {
		return getNOccupied();
	}

	@Override
	public int totmsg() {
		return nMsgGlobal;
	}
}
