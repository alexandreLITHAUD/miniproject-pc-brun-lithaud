package prodcons.v6;

public interface Message {

	public void produireMessage();

	public void consommerMessage();

	public void afficherMessage();
	
	public int getNumberInBuffer();
	
	public void takeOneInBuffer();
	public void addOneInBuffer();

}
