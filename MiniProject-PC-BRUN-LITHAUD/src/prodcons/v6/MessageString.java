package prodcons.v6;

public class MessageString implements Message {
	private String message;
	private int nbOccurDansBuff;

	public MessageString(String m) {
		message = m;
		nbOccurDansBuff = 0;
	}

	@Override
	public void afficherMessage() {
		System.out.println(message);
	}

	@Override
	public void produireMessage() {
	}

	@Override
	public void consommerMessage() {
		afficherMessage();
	}
	
	public int getNumberInBuffer() {
		return nbOccurDansBuff;
	}

	
	public void takeOneInBuffer() {
		nbOccurDansBuff --;
	}
	
	public void addOneInBuffer() {
		nbOccurDansBuff ++;
	}
}
