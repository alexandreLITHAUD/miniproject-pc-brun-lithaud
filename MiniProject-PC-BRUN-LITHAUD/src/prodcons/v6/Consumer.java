package prodcons.v6;

import java.util.Random;

public class Consumer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int consTime;
	private Random r;
	
	private int ratioThreadgetN; //chance de faire un getk plutot qu'un get
	private int kMax;// k max pour ce get k, 
	
	// Variable de teste qui sert a vérifier si les message ont bien été traités
	public static int nbTraite = 0;

	public Consumer(int consTime, int ratioGetK, int maxGetK, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initialisation des variables
		prodCons = pc;
		this.name = name;
		
		ratioThreadgetN = ratioGetK;
		kMax = maxGetK;
		
		//on set en demon pour permettre la fin du programme quand tous les producteurs auront finis
		thr.setDaemon(true);
		r = new Random();

		//on lance le thread
		thr.start();
	}
	
	public synchronized static void incr() {
		nbTraite++;
	}

	@Override
	public void run() {
		while (true) {
			Message mCourant;
			try {
				//lecture de k messages
				if(r.nextInt()%ratioThreadgetN == 1) {
					//on tire combien de messages on va lire
					int k = r.nextInt(kMax);
					Message[] mtab = prodCons.get(k);
					System.out.println("Thread : " + name + " traite "+k+" messages !");
					//traitement des k messages
					for(int i=0;i<k;i++) {
						mtab[i].consommerMessage();
					}
				}else {//lecture d'un SEUL message
					System.out.println("Thread : " + name + " traite un message : ");
					mCourant = prodCons.get();
					mCourant.consommerMessage();
				}

				//attente
				Thread.sleep(consTime);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}
}
