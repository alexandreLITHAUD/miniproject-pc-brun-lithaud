package prodcons.v6;

import java.util.Random;

public class Producer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int nbAProd;
	private int prodTime;
	
	private Random r;
	private int ratioThreadputN; //chance de faire un putk plutot qu'un put
	private int kMax; // k max pour ce put k, doit etre inférieur au nb de consumer

	public Producer(int minProd, int maxProd, int ratioPutK, int maxPutK, int pt, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initialisation des variables
		prodCons = pc;
		this.name = name;
		this.prodTime = pt;
		ratioThreadputN = ratioPutK;
		kMax = maxPutK;

		r = new Random();
		//tirage du nombre de messages a produire
		nbAProd = r.nextInt(maxProd - minProd) + minProd;
		
		//lancement du thread
		thr.start();
	}

	@Override
	public void run() {
		Message m;
		// On faire nbAProds messages 
		for (int i = 0; i < nbAProd; i++) {
			//création d'un message
			m = new MessageString(name + thr.getId());
			try {
				// Gestion du putk : si on tire un 1, on fait un put k
				if(r.nextInt(ratioThreadputN) == 1) {
					// On tire le nombre de fois ou sera dupliqué le message
					int k = r.nextInt(kMax);
					System.out.println("Thread : " + name + " produit "+k+" messages :");
					m.produireMessage();
					//on dépose les messages
					prodCons.put(m, k);
				}else { //sinon on fait un simple put
					System.out.println("Thread : " + name + " produit un message :");
					m.produireMessage();
					//dépot du message
					prodCons.put(m);
				}
				
				//mise en pause
				Thread.sleep(prodTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}

}
