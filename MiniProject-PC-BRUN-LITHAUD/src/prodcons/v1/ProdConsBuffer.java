package prodcons.v1;

public class ProdConsBuffer implements IProdConsBuffer {

	//nombre de place libres dans la structure
	private int nFree;

	//pointeur sur les positions d'ajout et de suppr 	
	private int in, out;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;

	//tableu des messages
	private Message[] msg;


	// Tableau de garde
	/*
	 * --------------------------------------------------------------------------
	 * - | Opérations | Pré-Action | Garde 				 | Post-action			|
	 * --------------------------------------------------------------------------
	 * - | Put 		  |	           | nFree > 0  		 | nFree-- & ajout msg  |
	 * --------------------------------------------------------------------------
	 * - | Get 		  |            | bufSz - nFree  > 0  | nFree ++	& suppr msg	|
	 * --------------------------------------------------------------------------
	 * 
	 */

	//fonction donnant le nombre de cases occupés
	private int getNOccupied() {
		return bufSz - nFree;
	}

	public ProdConsBuffer(int bufSz) {
		this.bufSz = bufSz;
		nFree = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
	}

	@Override
	public synchronized void put(Message m) throws InterruptedException {
		//tan qu'il n'y a pas de place libre on attend
		while (!(nFree > 0)) {
			wait();
		}

		//traitement du message
		msg[in] = m;
		in = (in + 1) % bufSz;
		//ajout d'un element
		nFree--;

		//on incrémente le nombre de message global
		nMsgGlobal++;
		//réveil des autres threads
		notifyAll();
	}

	@Override
	public synchronized Message get() throws InterruptedException {
		//tan qu'il n'y pas de message on attend
		while (!(getNOccupied() > 0)) {
			wait();
		}

		//ajout du messgae
		Message m = msg[out];
		out = (out + 1) % bufSz;

		//recuperation d'un element
		nFree++;
		//réveil des autres threads
		notifyAll();
		return m;
	}

	@Override
	public int nmsg() {
		return getNOccupied();
	}

	@Override
	public int totmsg() {
		return nMsgGlobal;
	}

}
