package prodcons.v1;

public class Consumer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int consTime;

	public Consumer(int consTime, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initilisation des variable
		prodCons = pc;
		this.name = name;

		//lancement du thread
		thr.start();
	}

	@Override
	public void run() {
		while (true) {
			Message mCourant;
			//on recupere un message et on le traite
			try {
				System.out.println("Thread : " + name + " traite le message :");
				mCourant = prodCons.get();

				mCourant.consommerMessage();

				Thread.sleep(consTime);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}
}
