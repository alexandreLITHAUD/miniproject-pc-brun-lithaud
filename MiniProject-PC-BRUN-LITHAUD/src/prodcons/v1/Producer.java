package prodcons.v1;

import java.util.Random;

public class Producer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int nbAProd;
	private int prodTime;

	public Producer(int minProd, int maxProd, int pt, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initilisation des variables
		prodCons = pc;
		this.name = name;
		this.prodTime = pt;

		Random rand = new Random();
		//tirage du nombre de message a produire
		nbAProd = rand.nextInt(maxProd - minProd) + minProd;

		//lancement du thread
		thr.start();
	}

	@Override
	public void run() {
		Message m;
		for (int i = 0; i < nbAProd; i++) {

			//on envoie un nouveau message dans le buffer
			m = new MessageString(name + thr.getId());
			try {
				m.produireMessage();
				prodCons.put(m);
				Thread.sleep(prodTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}

}
