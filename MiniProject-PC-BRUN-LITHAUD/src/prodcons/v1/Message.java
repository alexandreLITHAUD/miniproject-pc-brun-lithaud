package prodcons.v1;

public interface Message {

	public void produireMessage();

	public void consommerMessage();

	public void afficherMessage();

}
