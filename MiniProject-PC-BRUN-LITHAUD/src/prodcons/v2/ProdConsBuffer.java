package prodcons.v2;

public class ProdConsBuffer implements IProdConsBuffer {

	//nombre de places libres dans la structure
	private int nFree; // nempty

	//pointeurs sur les positions d'ajout et de suppr
	private int in, out;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;

	// tableau des messages
	private Message[] msg;

	//fonction donnant le nombre de cases occupés
	private int getNOccupied() {
		return bufSz - nFree;
	}

	public ProdConsBuffer(int bufSz) {
		this.bufSz = bufSz;
		nFree = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
	}

	@Override
	public synchronized void put(Message m) throws InterruptedException {
		//tan qu'il n'y a pas de place libre on attend
		while (!(nFree > 0)) {
			wait();
		}

		//traitement du message
		msg[in] = m;
		in = (in + 1) % bufSz;

		//on ajoute un élément
		nFree--;

		//On met a jour le nombre globale de message
		nMsgGlobal++;

		//réveil des autres threads
		notifyAll();
	}

	@Override
	public synchronized Message get() throws InterruptedException {
		//tan qu'il n'y a pas de message on attend
		while (!(getNOccupied() > 0)) {
			wait();
		}

		//on recupere le dernier message
		Message m = msg[out];
		out = (out + 1) % bufSz;

		//libération de l'espace
		nFree++;

		//réveils des autres threads
		notifyAll();
		return m;
	}

	@Override
	public int nmsg() {
		return getNOccupied();
	}

	@Override
	public int totmsg() {
		return nMsgGlobal;
	}

}
