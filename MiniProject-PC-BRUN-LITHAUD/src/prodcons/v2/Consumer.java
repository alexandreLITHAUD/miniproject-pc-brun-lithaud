package prodcons.v2;

public class Consumer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int consTime;
	
	// Variable de teste qui sert a vérifier si les message ont bien été traités
	public static int nbTraite = 0;

	public Consumer(int consTime, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initailisation des variables
		prodCons = pc;
		this.name = name;

		//on set en demon pour permettre la fin du programme quand tous les producteurs auront finis
		thr.setDaemon(true);

		//on lance le thread
		thr.start();
	}
	
	public synchronized static void incr() {
		nbTraite++;
	}

	@Override
	public void run() {
		while (true) {
			Message mCourant;
			//lecture et traitement des messages
			try {
				System.out.println("Thread : " + name + " traite le message :");
				mCourant = prodCons.get();

				mCourant.consommerMessage();
				incr();

				Thread.sleep(consTime);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}
}
