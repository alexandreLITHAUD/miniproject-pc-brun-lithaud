package prodcons.v4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProdConsBuffer implements IProdConsBuffer {
	//pointeurs sur les positions d'ajout et de suppr
	private int in, out;
	
	//nombre de places libres dans la structure
	private int nFree;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;
	
	// tableau des messages
	private Message[] msg;
	
	//locké conditionnels
	private Lock lock;
	private Condition notFull;
	private Condition notEmpty;

	public ProdConsBuffer(int bufSz) {
		//init des variables
		this.bufSz = bufSz;
		nFree = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
		
		//creation des locks/conditions
		lock = new ReentrantLock();
		notFull  = lock.newCondition(); 
		notEmpty = lock.newCondition(); 
	}

	//fonction donnant le nombre de cases occupés
	private int getNOccupied() {
		return bufSz - nFree;
	}

	@Override
	public void put(Message m) throws InterruptedException {
		//acquisition
		lock.lock();
		try {
			//tq pas de place
			while (!(nFree > 0)) {
				//attente sur la condition notFull
				notFull.await();
			}
			//gestion du message
			msg[in] = m;
			in = (in + 1) % bufSz;
			nMsgGlobal++;
			nFree--;
			//avertissement qu'un objet vient d'etre ajouté a notEmpty
			notEmpty.signal();
		}finally {
			//dans tous les cas, on unlock
			lock.unlock();
		}
	}

	@Override
	public Message get() throws InterruptedException {
		//acquisition
		lock.lock();
		try {
			//tq buff vide
			while (!(getNOccupied()>0)) {
				//attente sur la condition notEmpty
				notEmpty.await();
			}
			//gestion du message
			Message m = msg[out];
			out = (out + 1) % bufSz;
			nFree++;
			//avertissement qu'une place c'est libererr
			notFull.signal();
			return m;
		}finally{
			//on rends le lock
			lock.unlock();
		}		
	}

	@Override
	public int nmsg() {
		return getNOccupied();
	}

	@Override
	public int totmsg() {
		return nMsgGlobal;
	}

}
