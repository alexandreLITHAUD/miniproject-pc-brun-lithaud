package prodcons.v5;

import java.util.Random;

public class Producer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int nbAProd;
	private int prodTime;

	public Producer(int minProd, int maxProd, int pt, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//initialisation des variables
		prodCons = pc;
		this.name = name;
		this.prodTime = pt;

		Random rand = new Random();
		//tirage du nombre de messages a produire
		nbAProd = rand.nextInt(maxProd - minProd) + minProd;
		
		//lancement du thread
		thr.start();
	}

	@Override
	public void run() {
		Message m;
		// On faire nbAProds messages 
		for (int i = 0; i < nbAProd; i++) {
			//création d'un message
			m = new MessageString(name + thr.getId());
			try {
				// production du message puis put de celui-ci
				m.produireMessage();
				prodCons.put(m);
				//mise en pause
				Thread.sleep(prodTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}

}
