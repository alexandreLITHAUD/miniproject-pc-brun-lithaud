package prodcons.v5;

public interface Message {

	public void produireMessage();

	public void consommerMessage();

	public void afficherMessage();

}
