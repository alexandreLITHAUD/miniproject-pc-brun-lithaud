package prodcons.v5;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProdConsBuffer implements IProdConsBuffer {
	//pointeurs sur les positions d'ajout et de suppr
	private int in, out;
	
	//nombre de places libres dans la structure
	private int nFree;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;
	
	// tableau des messages
	private Message[] msg;

	//bools de gestion de synchro
	private boolean InGetK = false;

	public ProdConsBuffer(int bufSz) {
		//init des variables
		this.bufSz = bufSz;
		nFree = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
	}

	//fonction donnant le nombre de cases occupés
	private synchronized int getNOccupied() {
		return bufSz - nFree;
	}

	@Override
	public synchronized void put(Message m) throws InterruptedException {
		//tq il n'y a pas de places libres 
		while (!(nFree > 0)) {
			wait();
		}

		//traitement d'un message
		msg[in] = m;
		in = (in + 1) % bufSz;
		nMsgGlobal++;
		//on a ajouté un élément
		nFree--;

		//réveil des autres threads
		notifyAll();
	}

	@Override
	public synchronized Message get() throws InterruptedException {
		// Tq il n'y a aucun élément ou qu'un thread est dans le qetK
		while ((!(getNOccupied()>0)) || InGetK) {
			wait();
		}

		//getion du message 
		Message m = msg[out];
		msg[out] = null;
		out = (out + 1) % bufSz;
		//libération de l'espace
		nFree++;
		//pour le debug, on avertis qu'on a lu 1 message
		Consumer.incr();
		//libération des blo
		notifyAll();
		return m;
	}

	@Override
	public synchronized Message[] get(int k) throws InterruptedException {
		//tq quelqu'un est deja dans le getK
		while(InGetK) {
			wait();
		}

		//entrée
		InGetK = true;
		Message[] messagesK = new Message[k];
		//on recupère k messages :
		for(int i=0;i<k;i++) {
			//si on peut récup
			while (!(getNOccupied()>0)) {
				wait();
			}
			//on recupère
			messagesK[i] = msg[out];
			msg[out] = null;
			out = (out + 1) % bufSz;
			nFree++;
			//pour le debug, on avertis qu'on a lu 1 message
			Consumer.incr();
			//libération des producteurs
			notifyAll();
		}
		//sortie du getK
		InGetK = false;
		notifyAll();
		return messagesK;
	}

	@Override
	public synchronized int nmsg() {
		return getNOccupied();
	}

	@Override
	public synchronized int totmsg() {
		return nMsgGlobal;
	}
}
