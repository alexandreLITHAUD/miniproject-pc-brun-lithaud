package prodcons.v3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class TestProdCons {

	public static void main(String[] args) {
		/* Initialisation des valeurs de variables */
		Properties prop = new Properties();
		int nProd, nCons, bufSz, prodTime, consTime, minProd, maxProd;
		try {
			prop.loadFromXML(TestProdCons.class.getClassLoader().getResourceAsStream("options.xml"));
			nProd = Integer.parseInt(prop.getProperty("nProd"));
			nCons = Integer.parseInt(prop.getProperty("nCons"));
			bufSz = Integer.parseInt(prop.getProperty("bufSz"));
			prodTime = Integer.parseInt(prop.getProperty("prodTime"));
			consTime = Integer.parseInt(prop.getProperty("consTime"));
			minProd = Integer.parseInt(prop.getProperty("minProd"));
			maxProd = Integer.parseInt(prop.getProperty("maxProd"));
		} catch (IOException e) {
			System.err.println("Impossible de charger le fichier option, les valeurs par défaut vont etres prises !");
			nProd = 15;
			nCons = 10;
			bufSz = 1;
			prodTime = 10;
			consTime = 10;
			minProd = 5;
			maxProd = 500;
		}

		/* Création du buffer partagé */
		IProdConsBuffer buff = new ProdConsBuffer(bufSz);

		/* création des Consumer */
		ArrayList<Consumer> listCons = new ArrayList<>();
		Consumer c;
		for (int i = 0; i < nCons; i++) {
			c = new Consumer(consTime, buff, String.valueOf(i));
			listCons.add(c);
		}

		/* création des Producers */
		ArrayList<Producer> listProd = new ArrayList<>();
		Producer p;
		for (int i = 0; i < nProd; i++) {
			p = new Producer(minProd, maxProd, prodTime, buff, String.valueOf(i));
			listProd.add(p);
		}

		/* attente des Producer */
		for (Producer pr : listProd) {
			try {
				pr.join();
			} catch (InterruptedException e) {
				System.err.println("Erreur durant attente d'un producer : " + pr.getName());
				e.printStackTrace();
			}
		}

		/* attente des Consumer, ce sont des démons qui terminerons quand tous les messages seronts
		 * consommés. Une autre méthode consiste a utilisé les interuptions */
		while(buff.nmsg() != 0) {
			Thread.yield();
		}
			
		// NB MESSAGE PRODUIT
		System.out.println("\nNB MESSAGE PRODUIT : " + buff.totmsg());
		
		// NB MESSAGE TRAITE
		System.out.println("NB MESSAGE TRAITE : " + Consumer.nbTraite);
		
		if(buff.totmsg() != Consumer.nbTraite) {			
			System.err.println("SYNCHONIZATION PROBLEM");
		}else {
			System.out.println("That's all folks !!");
		}

	}

}
