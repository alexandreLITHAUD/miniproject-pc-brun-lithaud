package prodcons.v3;

import java.util.concurrent.Semaphore;

public class ProdConsBuffer implements IProdConsBuffer {
	//pointeurs sur les positions d'ajout et de suppr
	private int in, out;

	//taille du buffer d'entrée
	private int bufSz;

	//nombre total de messages
	private int nMsgGlobal;
	
	//tableau des messages
	private Message[] msg;
	
	//Semaphore de gestion de j'ajout 
	private Semaphore notFull;
	//Semaphore de gestion de la récuperation
	private Semaphore notEmpty;
	
	//mutex qui va agir comme une section critique
	private Semaphore mutex = new Semaphore(1);

	public ProdConsBuffer(int bufSz) {
		this.bufSz = bufSz;
		in = 0;
		out = 0;
		msg = new Message[bufSz];
		nMsgGlobal = 0;
		
		//init des sémaphores
		this.notFull = new Semaphore(bufSz);
		this.notEmpty = new Semaphore(0);
		
	}

	@Override
	public void put(Message m) throws InterruptedException {

		//si le buffer est plein alors le sémaphore sera bloquant
		this.notFull.acquire();
		
		//le mutex agit comme une section critique
		this.mutex.acquire();
		
		//traitement du message
		msg[in] = m;
		in = (in + 1) % bufSz;
		//incrementation du nombre de message global
		nMsgGlobal++;
		
		//on libere la section critique
		this.mutex.release();
		
		//on offre une ressouce/crédit au sémaphore de récuperation
		this.notEmpty.release();
	}

	@Override
	public Message get() throws InterruptedException {

		//si le buffer est vide alors le sémaphore sera bloquant
		this.notEmpty.acquire();
		
		//le mutex agit comme une section critique
		this.mutex.acquire();
		
		//on recupére le message
		Message m = msg[out];
		out = (out + 1) % bufSz;
		
		//on libere la section critique
		this.mutex.release();
		
		//on offre une ressouce/crédit au sémaphore d'ajout
		this.notFull.release();

		return m;
	}

	@Override
	public int nmsg() {
		return Math.abs((out-in));
	}

	@Override
	public int totmsg() {
		return nMsgGlobal;
	}

}
