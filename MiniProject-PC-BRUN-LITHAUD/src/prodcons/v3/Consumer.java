package prodcons.v3;

public class Consumer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int consTime;
	
	// Variable de teste qui sert a vérifier si les message ont bien été traités
	public static int nbTraite = 0;

	public Consumer(int consTime, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		prodCons = pc;
		this.name = name;

		//gestion de la terminaison
		thr.setDaemon(true);

		//lancement du thread
		thr.start();
	}
	
	public synchronized static void incr() {
		nbTraite++;
	}

	@Override
	public void run() {
		while (true) {
			Message mCourant;
			try {
				System.out.println("Thread : " + name + " traite le message :");
				//recup messages
				mCourant = prodCons.get();

				//consommation
				mCourant.consommerMessage();
				incr();

				//pause
				Thread.sleep(consTime);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}
}
