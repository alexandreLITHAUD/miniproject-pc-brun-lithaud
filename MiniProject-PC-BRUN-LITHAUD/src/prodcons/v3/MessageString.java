package prodcons.v3;

public class MessageString implements Message {
	private String message;

	public MessageString(String m) {
		message = m;
	}

	@Override
	public void afficherMessage() {
		System.out.println(message);
	}

	@Override
	public void produireMessage() {
	}

	@Override
	public void consommerMessage() {
		afficherMessage();
	}

}
