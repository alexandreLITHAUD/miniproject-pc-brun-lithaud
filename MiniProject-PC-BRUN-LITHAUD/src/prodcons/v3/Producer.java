package prodcons.v3;

import java.util.Random;

public class Producer implements Runnable {
	private Thread thr;
	private IProdConsBuffer prodCons;
	private String name;

	private int nbAProd;
	private int prodTime;

	public Producer(int minProd, int maxProd, int pt, IProdConsBuffer pc, String name) {
		thr = new Thread(this);
		//init des variables
		prodCons = pc;
		this.name = name;
		this.prodTime = pt;

		Random rand = new Random();
		//tirage du nombre de puts
		nbAProd = rand.nextInt(maxProd - minProd) + minProd;
		thr.start();
	}

	@Override
	public void run() {
		Message m;
		//nAProd puts
		for (int i = 0; i < nbAProd; i++) {
			m = new MessageString(name + thr.getId());
			try {
				//production
				m.produireMessage();
				//sockage
				prodCons.put(m);
				//pause
				Thread.sleep(prodTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void join() throws InterruptedException {
		thr.join();
	}

	public String getName() {
		return name;
	}

}
