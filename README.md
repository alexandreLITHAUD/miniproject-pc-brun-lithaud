# Projet Java - Producteur / Consommateurs
> auteurs : Aurélie Avanturier, Samuel Brun, Alexandre Lithaud

***Ce projet est uniquement destiné à des fins éducatives***

L'objectif de ce projet est de comprendre et de mettre en place des solutions implementations de la structure de données Producteur Consommateur dans un cadre multi-threadé.

---

- [Projet Java - Producteur / Consommateurs](#projet-java---producteur--consommateurs)
  - [Objectif 1 – Solution directe : lien du package](#objectif-1--solution-directe--lien-du-package)
  - [Objectif 2 – Terminaison : lien du package](#objectif-2--terminaison--lien-du-package)
  - [Objectif 3 – Solution basée sémaphores : lien du package](#objectif-3--solution-basée-sémaphores--lien-du-package)
  - [Objectif 4 – Solution basée sur les Locks et Conditions de Java \[optionnel\] : lien du package](#objectif-4--solution-basée-sur-les-locks-et-conditions-de-java-optionnel--lien-du-package)
  - [Objectif 5 – Multi-consommation : lien du package](#objectif-5--multi-consommation--lien-du-package)
  - [Objectif 6 – Multi-exemplaires synchrone : lien du package](#objectif-6--multi-exemplaires-synchrone--lien-du-package)


---

**Liens du sujet :** [Projet-ProdCons.pdf](./Projet-ProdCons.pdf)


## Objectif 1 – Solution directe : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v1/)

## Objectif 2 – Terminaison : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v2/)

## Objectif 3 – Solution basée sémaphores : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v3/)

## Objectif 4 – Solution basée sur les Locks et Conditions de Java [optionnel] : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v4/)

## Objectif 5 – Multi-consommation : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v5/)

## Objectif 6 – Multi-exemplaires synchrone : [lien du package](./MiniProject-PC-BRUN-LITHAUD/src/prodcons/v6/)

